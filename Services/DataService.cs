﻿
using Core;
using Newtonsoft.Json.Linq;
using Services.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Services
{
    public class DataService : IDataService
    {
        private const string _apiUrl = "https://clinicaltables.nlm.nih.gov/api/npi_idv/v3/search";

        private readonly IHttpClientFactory _clientFactory;

        public DataService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IEnumerable<JObject>> GetData(FilterCommand command, CancellationToken cancellationToken = default)
        {
            var displayFields = string.Join(",", command.DisplayFields);
            var searchFields = string.Join(",", command.SearchFields);
            var additionalFields = string.Join(",", command.AdditionalFields);

            var urlParams = $"{_apiUrl}?" +
                $"terms={command.Terms}" +
                $"&maxList={command.MaxList}" +
                $"&df={displayFields}" +
                $"&sf={searchFields}" +
                $"&ef={additionalFields}";

            using var client = _clientFactory.CreateClient();
            using var response = await client.GetAsync(urlParams, cancellationToken);

            var result = new List<JObject>();

            if (response.IsSuccessStatusCode)
            {
                var arrayResult = JArray.Parse(await response.Content.ReadAsStringAsync());
                var data = arrayResult[3] as JArray;

                bool TryGetJsonValue(JToken input, out JToken output)
                {
                    output = null;

                    if (string.IsNullOrEmpty(input.ToString())) return false;

                    try
                    {
                        var array = JArray.Parse($"[{input}]");
                        if (array.Count == 1)
                        {
                            output = array[0];
                        }
                        else
                        {
                            output = array;
                        }

                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }

                foreach (var item in data)
                {
                    var obj = new JObject();
                    for (int i = 0; i < command.DisplayFields.Length; i++)
                    {
                        obj.Add(command.DisplayFields[i], TryGetJsonValue(item[i], out var output) ? output : item[i]);
                    }

                    //result.Add(obj.ToObject<DataDTO>());
                    result.Add(obj);
                }
            }

            return result;
        }
    }
}
