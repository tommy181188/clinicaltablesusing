﻿using Core;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDataService
    {
        Task<IEnumerable<JObject>> GetData(FilterCommand command, CancellationToken cancellationToken = default);
    }
}
