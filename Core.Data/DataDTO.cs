﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Data
{
    public class TaxonomyDTO
    {
        public string Code { get; set; }

        public string Grouping { get; set; }

        public string Classification { get; set; }

        public string Specialization { get; set; }
    }

    public class MedicareDTO
    {
        [JsonProperty("spc_code")]
        public string SpcCode { get; set; }

        public string Type { get; set; }
    }

    public class LicensesDTO
    {
        public TaxonomyDTO Taxonomy { get; set; }

        public IEnumerable<MedicareDTO> Medicare { get; set; }
    }

    public class NameDTO
    {
        public string Last { get; set; }

        public string First { get; set; }

        public string Middle { get; set; }

        public string Credential { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }
    }

    public class AddressDTO
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Country { get; set; }
    }

    public class OtherIdsDTO
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public string Issuer { get; set; }

        public string State { get; set; }
    }

    public class AuthDTO
    {
        public string Last { get; set; }

        public string First { get; set; }

        public string Middle { get; set; }

        public string Credential { get; set; }

        public string Title { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public string Phone { get; set; }
    }

    public class MiscDTO
    {
        [JsonProperty("auth_official")]
        public AuthDTO AuthOfficial { get; set; }

        [JsonProperty("replacement_NPI")]
        public string ReplacementNPI { get; set; }

        public string EIN { get; set; }

        [JsonProperty("enumeration_date")]
        public string EnumerationDate { get; set; }

        [JsonProperty("last_update_date")]
        public string LastUpdateDate { get; set; }

        [JsonProperty("is_sole_proprietor")]
        public string IsSoleProprietor { get; set; }

        [JsonProperty("is_org_subpart")]
        public string IsOrgSubpart { get; set; }

        [JsonProperty("parent_LBN")]
        public string ParentLBN { get; set; }

        [JsonProperty("parent_TIN")]
        public string ParentTIN { get; set; }
    }

    public class DataDTO
    {
        public string NPI { get; set; }

        [JsonProperty("provider_type")]
        public string ProviderType { get; set; }

        public string Gender { get; set; }

        public LicensesDTO Licenses { get; set; }

        public NameDTO Name { get; set; }

        [JsonProperty("name.full")]
        public string NameFull { get; set; }

        [JsonProperty("addr_practice")]
        public AddressDTO AddrPractice { get; set; }

        [JsonProperty("addr_practice.full")]
        public string AddrPractiseFull { get; set; }

        [JsonProperty("addr_mailing")]
        public AddressDTO AddrMailing { get; set; }

        [JsonProperty("addr_mailing.full")]
        public string AddrMailingFull { get; set; }

        [JsonProperty("name_other")]
        public NameDTO NameOther { get; set; }

        [JsonProperty("name_other.full")]
        public string NameOtherFull { get; set; }

        [JsonProperty("other_ids")]
        public IEnumerable<OtherIdsDTO> OtherIds { get; set; }

        public MiscDTO Misc { get; set; }
    }
}
