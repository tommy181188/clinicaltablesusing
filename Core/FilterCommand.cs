﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class FilterCommand
    {
        public string Terms { get; set; }

        public int MaxList { get; set; } = 500;

        public string[] DisplayFields { get; set; } = Array.Empty<string>();

        public IEnumerable<string> SearchFields { get; set; } = Array.Empty<string>();

        public IEnumerable<string> AdditionalFields { get; set; } = Array.Empty<string>();
    }
}
