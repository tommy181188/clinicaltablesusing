﻿using Core.Data;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace WebApplication4.Models
{
    public class DataModel
    {
        public int CurrentPage { get; set; }

        public int PageCount { get; set; }

        public int FirstRowOnPage { get; set; }

        public int LastRowOnPage { get; set; }

        public int RowCount { get; set; }

        //public IEnumerable<string> AllDisplayFields { get; set; }
        //public IEnumerable<string> DisplayFields { get; set; }
        public IEnumerable<JObject> Data { get; set; }
    }
}
