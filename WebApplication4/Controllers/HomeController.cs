﻿using Core;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication4.Helpers;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDataService _dataService;

        public HomeController(IDataService dataService) => _dataService = dataService;

        public async Task<IActionResult> Index(int? page, string terms, CancellationToken cancellationToken)
        {
            var command = new FilterCommand
            {
                Terms = terms,
                DisplayFields = new[] { "NPI", "name.full" },
                SearchFields = new[] { "name.full" }
            };
            return View((await _dataService.GetData(command, cancellationToken)).GetPaged(page ?? 1, 20));
        }

        [Route("[action]/{npi}")]
        public async Task<IActionResult> Item(int npi, CancellationToken cancellationToken)
        {
            var command = new FilterCommand
            {
                MaxList = 1,
                DisplayFields = GetDisplayFields(),
                SearchFields = new[] { "NPI" },
                Terms = npi.ToString()
            };

            ViewData["DisplayFields"] = command.DisplayFields;

            return View((await _dataService.GetData(command, cancellationToken)).SingleOrDefault());
        }

        public async Task<IActionResult> FullSearch(int? page, [FromQuery] FilterCommand command, CancellationToken cancellationToken)
        {
            ViewData["AllDisplayFields"] = GetDisplayFields();
            ViewData["DisplayFields"] = command.DisplayFields;

            command.SearchFields = command.DisplayFields;

            return View((await _dataService.GetData(command, cancellationToken)).GetPaged(page ?? 1, 10));
        }

        private string[] GetDisplayFields() => new[]
                {
                    "NPI",
                    "provider_type",
                    "gender",
                    "licenses",
                    "name",
                    "name.full",
                    "addr_practice",
                    "addr_practice.full",
                    "addr_mailing",
                    "addr_mailing.full",
                    "name_other",
                    "name_other.full",
                    "other_ids",
                    "misc"
                };
    }
}
